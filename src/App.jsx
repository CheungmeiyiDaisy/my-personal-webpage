import React from 'react'
import Header from './components/header/Header'
import Nav from './components/nav/Nav'
import About from './components/about/About'
import Internship from './components/internship/Internship'
import Projects from './components/projects/Projects'
import Testinonials from './components/testimonials/Testimonials'
import Contact from './components/contact/Contact'

const App = () => {
  return (
    <>
       <Header />
       <Nav />
       <About />
       <Internship />
       <Projects />
       <Testinonials />
       <Contact />

    </>
  )
}

export default App