import React from 'react'
import './nav.css'
import {IoIosHome} from 'react-icons/io'
import {BiUserCircle} from 'react-icons/bi'
import {GrCertificate} from 'react-icons/gr'
import {GrContact} from 'react-icons/gr'
import { useState } from 'react'

const Nav = () => {
 const[activeNav, setActiveNav] = useState('#')
 return (
    <nav>
        <a href="#" onClick={() => setActiveNav('#')} className={activeNav === '#' ? 'active':''} ><IoIosHome/></a>
        <a href="#about" onClick={() => setActiveNav('#about')} className={activeNav === '#about' ? 'active':''}
        ><BiUserCircle/></a>
        <a href="#projects" onClick={() => setActiveNav('#projects')} className={activeNav === '#projects' ? 'active':''} 
        ><GrCertificate/></a>
        <a href="#contact" onClick={() => setActiveNav('#contact')} className={activeNav === '#contact' ? 'active':''}
        ><GrContact/></a>


    </nav>
  )
}

export default Nav


