import React from 'react'
import './contact.css'
import {MdOutlineMarkEmailUnread} from 'react-icons/md'
import {TbMessage} from 'react-icons/tb'
const Contact = () => {
  return (
    <section id='contact'>
      <h5>Get In Touch</h5>
      <h2>Contact me</h2>

      <div className="container contact__container">
        <div className="contact__options">
          <article className="contact__option">
            <MdOutlineMarkEmailUnread/>
            <h4>Email</h4>
            <h5>cheungmeiyi914@gmail.com</h5>
            <a href="mailto:cheungmeiyi914@gmail.com" target="_blank">Send a Message</a>
          </article>

          <article className="contact__option">
            <TbMessage/>
            <h4>Messenger</h4>
            <h5>daisycheungg</h5>
            <a href="https://m.me/daisycheungg" target="_blank">Send a Message</a>
          </article>

        </div>
      </div>
    </section>
  )
}

export default Contact