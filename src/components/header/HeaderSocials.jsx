import React from 'react'
import {BsLinkedin} from 'react-icons/bs'
import {FaFacebookSquare} from 'react-icons/fa'
import {BsInstagram} from 'react-icons/bs'
import {IoLogoGitlab} from 'react-icons/io5'

const HeaderSocials = () => {
  return (
    <div className='header__socials'>
        <a href="https://facebook.com/daisycheungg" target="_blank"><FaFacebookSquare/></a>
        <a href="https://instagram.com/daisycheungg" target="_blank"><BsInstagram/></a>
        <a href="https://linkedin.com" target="_blank"><BsLinkedin/></a>
        <a href="https://gitlab.com/CheungmeiyiDaisy" target="_blank"><IoLogoGitlab/></a>
        </div>
  )
}

export default HeaderSocials