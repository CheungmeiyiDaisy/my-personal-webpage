import React from 'react'
import './about.css'
import ME from '../../assets/cry.jpg'
import {IoSchool} from 'react-icons/io5'
import {MdOutlineWork} from 'react-icons/md'
import {SiPytest} from 'react-icons/si'

const About =() => {
    return(
      <section id='about'>
        <h5> More </h5>
        <h2>About me</h2>

        <div className="container about__container">
            <div className="about__me">
                <div className="about__me-image">
                    <img src={ME} alt="About Image" />
                </div>
            </div>

        
            <div className="container about__content">
                 <div className="about__cards">
                    <article className='about__card'>
                        <IoSchool className='about__icon'/>
                        <h3>Education Background</h3>
                        <small>The Chinese Univerity of Hong Kong</small>
                    </article>

                    <article className='about__card'>
                        <MdOutlineWork className='about__icon'/>
                        <h3>Internship experience</h3>
                        <small>@Jump Intelligence Group</small>
                    </article>

                    <article className='about__card'>
                        <SiPytest className='about__icon'/>
                        <h3>Projects</h3>
                        <small>Using Python, Tableau, Excel </small>
                    </article>
                </div>

                <p>
                I am currently a year 3 student who is majoring in data science and policy studies 
                in CUHK. Throughout the study during university, I find out I am passionate with data science.
                My major equips me with some data science and data analytics knowledge. 
                It covers statistical analysis, using python for data cleaning and data analytic, 
                using Tableau for data visualization etc. It does not limit to data science, my major
                also equip us with the view and aspect on social science, it covers a wide range of topic
                including research study, innovation policy, innovation systems and policy analysis etc.
                Yet, the knowledge learnt from classes may be limited and I wish I can further study data 
                science with my interest. 
                </p>
            
                <a href="#contact" className='btn btn-primary'>Let's chat</a>
            </div>
        </div>
           
    </section>

    )
}

export default About